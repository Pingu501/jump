$(document).ready(function () {
	var calculationLoop;
	var screenWidth = $(document).width();
	var screenHeight = $(document).height();
	var calculationIsRunning = false;

	//set canvas size
	var canvas = document.getElementById('drawing');
	canvas.width = screenWidth
	canvas.height = screenHeight;
	var ctx = canvas.getContext('2d');


	$(document).on("keydown", function (e) {
		if (e.keyCode == "32") {
			generateNewElement();
		}
	});

	$('#header').on("click", function () {
		generateNewElement();
	});

	$('#header').on("taphold", function () {
		generateNewElement();
	});

	var element;

	var elementArray = [];

	var square = function () {
		var random = Math.random();
		this.position = Math.random() * screenWidth;
		this.gravity = random * 100;
		this.startSpeed = random * 600;
		this.startTime = new Date().getTime();

		switch (true) {
			case (random < 0.60):
				this.class = "small";
				this.color = "orangered";
				this.size = "50";
				break;
			case (random < 0.85):
				this.class = "medium";
				this.gravity = this.gravity * 2;
				this.color = "yellowgreen";
				this.size = "100";
				break;
			case (random >= 0.85):
				this.class = "big";
				this.gravity = this.gravity * 4;
				this.color = "deepskyblue";
				this.size = "200";
				break;
		}
	};

	function generateNewElement() {
		element = new square();
		elementArray.push(element);
		if (!calculationIsRunning) {
			calculationLoop = setInterval(function () { calculateElements(); }, 1);
		}
	}

	var deltaTime;
	var newElementHeight;
	var currentTime;
	var tempArray = [];

	function calculateElements() {
		clearInterval(calculationLoop);
		tempArray = [];
		currentTime = new Date().getTime();

		ctx.clearRect(0, 0, screenWidth, screenHeight);

		for (var i = 0; i <= (elementArray.length) - 1; i++) {
			element = elementArray[i];

			deltaTime = (currentTime - element.startTime) / 1000;
			newElementHeight = screenHeight - (element.startSpeed * deltaTime - (element.gravity / 2) * deltaTime * deltaTime);

			if (!(deltaTime > 5 && newElementHeight > screenWidth)) {
				ctx.beginPath();
				ctx.rect(element.position, newElementHeight, element.size, element.size);
				ctx.fillStyle = element.color;
				ctx.fill();

				tempArray.push(element);
			}
		}

		elementArray = tempArray;

		calculationLoop = setInterval(function () { calculateElements(); }, 1);
	}

});
